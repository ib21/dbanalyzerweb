<%-- 
    Document   : home2
    Created on : 14-Aug-2018, 14:25:02
    Author     : Graduate
--%>
<%
    if (session.getAttribute("session") != "TRUE") {
        response.sendRedirect("error.jsp");
    }
%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            * {
                box-sizing: border-box;
            }

            body {
                font-family: Arial, Helvetica, sans-serif;
            }

            /* Style the header */
            .header {
                background-color: #f1f1f1;
                padding: 50px;
                text-align: center;
                font-size: 20px;
            }
            .toolbar{
                float: right;
            }

            /* Create three equal columns that floats next to each other */
            .column {
                float: left;
                width: 50%;
                padding: 10px;
                height: 400px; /* Should be removed. Only for demonstration */
                text-align: center;
                vertical-align: middle;
                line-height: 400px;
            }

            /* Clear floats after the columns */
            .row:after {
                content: "";
                display: table;
                clear: both;
            }

            /* Style the footer */
            .footer {
                background-color: #f1f1f1;
                padding: 10px;
                text-align: center;
            }
            img {
                width: 350px;
                border-radius: 50%;
            }
            .centered {


                text-align: center;
            }

            /* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
            @media (max-width: 600px) {
                .column {
                    width: 100%;
                }
            }
        </style>
    </head>
    <body>
        <div class="header">
            <h2>Welcome Back <%=session.getAttribute("user_id")%></h2>
            <div class ="toolbar">
                <a href="logout.jsp"><button>Logout</button></a>
            </div>
        </div>
            
        <div class="row">
            <div class="column" style="background-color:#aaa;">
                <a href="index.jsp"><img src="image/datatable.png"></a>
            </div>
            <div class="column" style="background-color:#bbb;">
                <img src="image/datavis.png" alt="Correlation Data">
            </div>
        </div>

        <div class="footer">
            <p>produced by team B2.1</p>
        </div>

    </body>
</html>
