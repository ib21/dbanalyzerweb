/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var rootURL = "db";
$(document).ready(function () {
    tableSetup();
});
function constructTable() {
    console.log("constructTable");
    var result = $.ajax({
        type: 'GET',
        global: false,
        async: false,
        url: rootURL + '/getAllInstruments',
        dataType: "json", // data type of response
        success: function (data) {
            console.log("SUCCESS");
            console.log(data);
        },
        error: function (data) {
            console.log("FAILURE");
            console.log(data);
        }

    }).responseJSON;
    return result;

}

function tableSetup() {
    $('#t1').DataTable({
        "data": constructTable(),
        "columns": [
            {"data": "id"},
            {"data": "name"}
        ]
    });
}
//    var oldTable = document.getElementById('t1'),
//            newTable = oldTable.cloneNode();
//    for (var i = 0; i < data.length; i++) {
//        var tr = document.createElement('tr');
//        for (var j = 0; j < data[i].length; j++) {
//            var td = document.createElement('td');
//            td.appendChild(document.createTextNode(data[i][j]));
//            tr.appendChild(td);
//        }
//        newTable.appendChild(tr);
//    }
//
//    oldTable.parentNode.replaceChild(newTable, oldTable);
//});


