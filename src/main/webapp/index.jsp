<jsp:useBean id="globalHelper" class="com.db.ApplicationScopeHelper" scope="application"/>
<hr />
<!DOCTYPE html>
<html>
    <head>
        <link rel="icon" type="image/png" href="assets/img/db_logo.ico">
        <title>dbMarketVision</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
        <script src="hancUI/js/main.js"></script>

    </head>
    <body>  
        <%

            String dbStatus = "DB NOT CONNECTED";

            globalHelper.setInfo("Set any value here for application level access");
            boolean connectionStatus = globalHelper.bootstrapDBConnection();

            if (connectionStatus) {
                dbStatus = "You have successfully connected to the Deutsche Investment DB";
            }

            if (connectionStatus) {

                String profile_msg = (String) request.getAttribute("profile_msg");
                if (profile_msg != null) {
                    out.print(profile_msg);
                }
                String login_msg = (String) request.getAttribute("login_msg");
                if (login_msg != null) {
                    out.print(login_msg);
                }
        %>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script src="js/loginInput.js"></script>
    </head>
    <body style="background: url(image/bg.jpg) no-repeat center center fixed;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;">
        <div id="login">
            <div class="container">
                <div id="logo" class="row justify-content-center align-items-center">
                    <img src="image/logo.png" alt="dbMarketVision" height="300px" style="padding:40px;">
                </div>
                <div id="login-row" class="row justify-content-center align-items-center">
                    <div id="login-column" class="col-md-6">
                        <div id="login-box" class="col-md-12">
                            <form id="login-form" class="form" action="loginprocess.jsp" method="post">
                                <div class="form-group">
                                    <input type="text" name="user" id="username" class="form-control" title="Enter User Name">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="pass" id="password" class="form-control" title="Enter Password">
                                </div>
                                <div class="form-group text-center">
                                    <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                                </div>
                                <div id="forget-link" class="text-center">
                                    <a href="#" class="text-info">Forgot Username/Password</a>
                                </div>
                                <div id="register-link" class="text-center">
                                    <a href="#" class="text-info">Register as New User</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%
            }
        %>


    </body>
</html>

<br />
<!--
<form action="loginprocess.jsp" method="post">
        Email:<input type="text" name="user" /><br />
        <br /> Password:<input type="password" name="pass" /><br />
        <br /> <input type="submit" value="login" />"
</form>

<script>
<%
    if (session.getAttribute("session") == "TRUE") {
        response.sendRedirect("dashboard.jsp");
    }
%>
</script>