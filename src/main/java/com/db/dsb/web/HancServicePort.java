/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.dsb.web;

import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.db.DealDAO;
import com.db.InstrumentDAO;
import com.db.CounterpartyDAO;
import com.db.dsb.utils.SimpleJsonMessage;
import domain.*;
import javafx.util.Pair;

//import com.celestial.dsb.core.MetatagController;
//import com.celestial.dsb.core.URLController;
//import com.celestial.dsb.utils.SimpleJsonMessage;

//import domain.Bookmark;

/**
 *  
 * @author Selvyn
 */
@Path("/")
public class HancServicePort
{
	
    @GET
    @Path("/sayhello")
    public Response sayHtmlHelloTest()
    {
        String result = "<html> " + "<title>" + "hanc" + "</title>"
                + "<body><h1>" + "dsb is running..." + "</h1></body>" + "</html> ";

        return Response.status(200).entity(result).build();
    }
    
    @GET
    @Path("/getAllDeals")
    public Response getAllDeals()
    {
    	DealDAO da = new DealDAO();
        ArrayList<Deal> deals = da.getAllDeals();
        return Response.ok(deals, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @GET
    @Path("/getAllCounterparties")
    public Response getAllCounterparties()
    {
    	CounterpartyDAO cd = new CounterpartyDAO();
        ArrayList<Counterparty> parties = cd.getAllCounterparties();
        return Response.ok(parties, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @GET
    @Path("/getAllInstruments")
    public Response getAllInstruments()
    {
    	InstrumentDAO id = new InstrumentDAO();
        ArrayList<Instrument> instruments = id.getAllInstruments();
        Pair<String,Double> pair = new Pair("Sam", 4.5);
        Pair<String,Double> pair2 = new Pair("Samuel", 7.5);
        ArrayList<Pair> pairs = new ArrayList();
        pairs.add(pair);
        pairs.add(pair2);
        return Response.ok(instruments, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @GET
    @Path("/getInstrumentStats")
    public Response getInstrumentStats()
    {
    	InstrumentDAO id = new InstrumentDAO();
        ArrayList<InstrumentStat> stats = id.getInstrumentStatistics();
        return Response.ok(stats, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @GET
    @Path("/getInstrumentBuyQuantity")
    public Response getInstrumentBuyQuantity()
    {
    	InstrumentDAO id = new InstrumentDAO();
        ArrayList<Pair> bQuantity = id.getBuyQuantity();
        return Response.ok(bQuantity, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @GET
    @Path("/getInstrumentSellQuantity")
    public Response getInstrumentSellQuantity()
    {
    	InstrumentDAO id = new InstrumentDAO();
        ArrayList<Pair> sQuantity = id.getSellQuantity();
        return Response.ok(sQuantity, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @GET
    @Path("/getRealizedProfitLoss")
    public Response getRealizedProfitLoss()
    {
    	CounterpartyDAO cd = new CounterpartyDAO();
        ArrayList<Pair> rProfit = cd.getRealizedProfitLoss();
        return Response.ok(rProfit, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @GET
    @Path("/getEffectiveProfitLoss")
    public Response getRealizedProfit()
    {
    	CounterpartyDAO cd = new CounterpartyDAO();
        ArrayList<Pair> eProfit = cd.getEffectiveProfitLoss();
        return Response.ok(eProfit, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    
    /**
    @GET
    @Path("/getAllTags")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAllTags()
    {
    	MetatagController mc = new MetatagController();
    	ArrayList<String> res = mc.getAllTags();
    	return Response.ok(res, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @Override
    @GET
    @Path("/getAllURL")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAllURL()
    {
        // Use a controller to get all the links, return the result
        //++ insert code here
    	URLController uc = new URLController();
    	ArrayList<Bookmark> links = uc.getAllLinks();
    	return Response.ok(links, MediaType.APPLICATION_JSON_TYPE).build();
    }

    @Override
    @GET
    @Path("/get/{tags}")
    public Response getSavedURLWithInfo(@PathParam("tags")String tags)
    {
    	URLController suc = new URLController();
    	ArrayList<Bookmark> res = suc.getUrlForTag(tags);
    	return Response.ok(res, MediaType.APPLICATION_JSON_TYPE).build();
    }

    @Override
    @GET
    @Path("/save/{url}/{description}/{tags}")
    public Response saveNewURLWithInfo( @PathParam("url")String url,
                                        @PathParam("description")String description,
                                        @PathParam("tags")String tags )
    {
        URLController suc = new URLController();
        suc.saveURL(url, description, tags);
        return Response.status(200).entity( new SimpleJsonMessage( url + "//" + description + "//" + tags ) ).build();
    }

    @Override
    @POST
    @Path("/save")
    public Response saveNewURLWithInfoFromForm( @FormParam("url")String url,
                                        @FormParam("description")String description,
                                        @FormParam("tags")String tags )
    {
        URLController suc = new URLController();
        suc.saveURL(url, description, tags);
        return Response.status(200).entity( new SimpleJsonMessage( url + "//" + description + "//" + tags ) ).build();
    }*/

}
